<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::get('shepherds','SheperdController@getSheperds');
Route::get('{tel}/{sms}/registershepherd/{name}/{desc}/{lat}/{long}','SheperdController@registerSheperds');
Route::get('{tel}/{sms}/updatesheperd/{lat}/{long}','SheperdController@update');

Route::get('{tel}/{sms}/registeruser/{name}','UserController@registerUser');
Route::get('{tel}/{sms}/updateuser/{lat}/{long}/{rad}','UserController@update');

Route::get('{tel}/{sms}/poi/{lat}/{long}','poiController@getPoi');
Route::get('{tel}/{sms}/help/{desc}/{lat}/{long}','poiController@help');
Route::get('{tel}/{sms}/registerpoi/{desc}/{lat}/{long}/{type}','poiController@registerPoi');
Route::get('{tel}/{sms}/updatepoi/{poi_loc_id}/{desc}/{lat}/{long}/{type}/{answer}','poiController@updatePoi');