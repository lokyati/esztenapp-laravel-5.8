<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	// $config = array();
	// $config['center'] = '46.293328, 25.287034 ';
	// $config['zoom'] = '14';
	// $config['map_height'] = '500px';
	// $config['map_width'] = '500px';
	// $config['scrollwhel'] = false;

	// GMaps::initialize($config);

	// $marker['position'] = '46.293328, 25.287034';
	// $marker['infowindow_content'] = 'Juhasz';
	// GMaps::add_marker($marker);

	// $marker['position'] = '46.294413, 25.288254';
	// $marker['infowindow_content'] = 'Turazo';
	// // $marker['icon'] = '/eszten_app/public/img/marker2.png';
	// $marker['icon'] = 'http://maps.google.com/mapfiles/kml/paddle/grn-blank.png';
	// GMaps::add_marker($marker);

	// $map = GMaps::create_map();

	// echo $map['js'];
 // 	echo $map['html'];

 //    return view('welcome')->with('map',$map);
    return view('welcome');
});

Route::get('getUser','UserController@getUser');
Route::get('home','UserController@home');

Route::post('newuser', 'UserController@store');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/menu', function () {
	return view('menu');
});
