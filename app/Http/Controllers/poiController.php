<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Poi;
use App\PoiLocation;
use App\Sheperds;
use App\User;
use DB;

class poiController extends Controller
{
    public function getPoi(Request $request){
        echo $request['long'];
        echo "|";
        echo $request['lat'];
    }

    // public function help(Request $request){
    // 	// $user = User::where('tel',$request['tel'])->first();
    // 	echo "help";
    // 	echo ";";
    // 	echo $request['tel'];
    // 	echo "|";
    // 	echo $request['long'];
    // 	echo "|";
    // 	echo $request['lat'];
    // 	echo "|";
    // 	echo $request['desc'];
    //     echo ";";
    //     echo '0758621479';
    //     echo ";";
    //     echo '0798742564';
    // }

    public function registerPoi(Request $request){
        if($request['sms'] === 1){
            //sms
        }else if($request['sms'] === 0){
            //server
        }
    	$data['pois_id'] = $request['type'];
        $data['description'] = $request['desc'];
        $data['longitude'] = $request['long'];
        $data['latitude'] = $request['lat'];
        $data['confirmed'] = 1;
        $data['denied'] = 0;
        $newpoi = new PoiLocation($data);
        $newpoi->save();
        echo "Poi saved!";
    }

    public function updatePoi(Request $request){
        if($request['sms'] === 1){
            //sms
        }else if($request['sms'] === 0){
            //server
        }
        $existingpoi = PoiLocation::where('id',$request['poi_loc_id'])->first();
        if($existingpoi){
        	if($request['answer'] === '1'){
        		$existingpoi['confirmed'] = $existingpoi['confirmed'] + 1;
        	}else if($request['answer'] === '0'){
        		$existingpoi['denied'] = $existingpoi['denied'] + 1;
        	}
        	$existingpoi['description'] = $request['desc'];
            $existingpoi->save();
            echo "Poi updated!!!";
        }else{
        	$data['pois_id'] = $request['type'];
	        $data['description'] = $request['desc'];
	        $data['longitude'] = $request['long'];
	        $data['latitude'] = $request['lat'];
	        $data['confirmed'] = 1;
	        $data['denied'] = 0;
	        $newpoi = new PoiLocation($data);
	        $newpoi->save();
	        echo "Poi saved!";
        }
    }

    public function help(Request $request){
        $updateuser = User::where('tel',$request['tel'])->first();
        $sheperds = Sheperds::all();


        $updateuser['longitude'] = $request['long'];
        $updateuser['latitude'] = $request['lat'];
        $updateuser->save();
        // select st_distance_sphere(POINT(-73.9949,40.7501), POINT( -73.9961,40.7542)) 

        $sheperds = DB::table('sheperds')
                ->selectRaw('id,name,description,tel,latitude,longitude, (6371 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin(radians(latitude)) ) ) AS distance', [$request['lat'], $request['long'], $request['lat']])
                ->havingRaw('distance < 10000')
                ->get();
        $users = DB::table('users')
                ->selectRaw('id,name,tel,latitude,longitude, (6371 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin(radians(latitude)) ) ) AS distance', [$request['lat'], $request['long'], $request['lat']])
                ->where("id",'!=',$updateuser->id)
                ->havingRaw('distance < 10000')
                ->get();
        // $pois = DB::table('poi_locations')
        //         ->selectRaw('id,description,latitude,longitude, (6371 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin(radians(latitude)) ) ) AS distance', [$request['lat'], $request['long'], $request['lat']])
        //         ->havingRaw('distance < '.$request['rad'])
        //         ->get();       
        $results = [
            "shepreds" => $sheperds,
            "users" => $users,
            // "pois" => $pois
        ];
        // $results = DB::select( DB::raw("SELECT ( 6371 * acos( cos( radians({$request['lat']}) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians({$request['long']}) ) + sin( radians({$request['lat']}) ) * sin(radians(latitude)) ) ) AS distance FROM sheperds HAVING distance < 25 ORDER BY distance"));
        //  foreach ($results as $result){
        //    var_dump($result);
        //  }

        // $results = DB::select( DB::raw("SELECT name,
        // ST_Distance_Sphere(POINT(46.326614, 25.454386), POINT(46.290002, 25.289924))
        // FROM sheperds"));
        // echo $results;
        //  foreach ($results as $result){
        //    var_dump($result);
        //  }

        


      //  echo "User updated!";
        // return $results;
            echo $updateuser->id;

            echo "help";
            echo ";";
            echo $request['tel'];
            echo "|";
            echo $request['lat'];
            echo "|";
            echo $request['long'];
            echo "|";
            echo $request['desc'];
            foreach ($sheperds as $key => $value) {
               echo ";";
               echo $value->tel;

            }
            foreach ($users as $key => $value) {
               echo ";";
               echo $value->tel;
            }
        
        
    }
}
