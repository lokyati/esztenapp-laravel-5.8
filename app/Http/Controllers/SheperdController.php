<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Sheperds;
use App\SheperdsHistory;
use App\Poi;
use App\PoiLocation;
use App\User;

class SheperdController extends Controller
{
    public function getSheperds(){
        $sheperd = Sheperds::all();
        $user = User::all();
        $pois = PoiLocation::all();
        return response()->json(array(
		    'shepherds' => $sheperd,
		    'users' => $user,
		    'pois' => $pois,
		));
    }

    public function registerSheperds(Request $request){
    	if($request['sms'] === 1){
    		//sms
    	}else if($request['sms'] === 0){
    		//server
    	}
    	$existingshepherd = Sheperds::where('tel',$request['tel'])->first();
    	if($existingshepherd){
    		$existingshepherd['name'] = $request['name'];
    		$existingshepherd['description'] = $request['desc'];
    		$existingshepherd['longitude'] = $request['long'];
    		$existingshepherd['latitude'] = $request['lat'];
    		$existingshepherd->save();
    		echo "Shepherd updated!";
    	}else{
    		$data['tel'] = $request['tel'];
    		$data['name'] = $request['name'];
    		$data['description'] = $request['desc'];
    		$data['longitude'] = $request['long'];
    		$data['latitude'] = $request['lat'];
    		$newshepherd = new Sheperds($data);
    		$newshepherd->save();
    		echo "Shepherd saved!";
    	}
    }

    public function update(Request $request){
    	$updatesheperd = Sheperds::where('tel',$request['tel'])->first();

    	$newhistory['sheperds_id'] = $updatesheperd['id'];
    	$newhistory['latitude'] = $updatesheperd['latitude'];
    	$newhistory['longitude'] = $updatesheperd['longitude'];
    	$newshepherdhistory = new SheperdsHistory($newhistory);
    	$newshepherdhistory->save();

		$updatesheperd['longitude'] = $request['long'];
		$updatesheperd['latitude'] = $request['lat'];
		$updatesheperd->save();
		echo "Shepherd updated and history created!";
    }
}
