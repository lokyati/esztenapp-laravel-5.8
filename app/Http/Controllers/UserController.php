<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Sheperds;
use DB;

class UserController extends Controller
{
    public function show($id){
    	$showuser = User::findOrfail($id);
    	return $showuser;         
        
    }

    public function home(){
    	return view('home');
    }

    public function getUser(){
        $user = Auth::user();
        return $user;
    }

    public function store(Request $request)
    {   
        $data = $request->all();
        $userdata['id'] = $data['tel'];
        $userdata['name'] = $data['name'];
        $user = new User($data);
        $user->save();
        return redirect('home');
    }

    public function registerUser(Request $request){
        if($request['sms'] === 1){
            //sms
        }else if($request['sms'] === 0){
            //server
        }
        $existinguser = User::where('tel',$request['tel'])->first();
        if($existinguser){
            $existinguser['name'] = $request['name'];
            $existinguser->save();
            echo "User updated!";
        }else{
            $data['tel'] = $request['tel'];
            $data['name'] = $request['name'];
            $newuser = new User($data);
            $newuser->save();
            echo "User saved!";
        }
    }
    private function roundCoords($items){
        return $items->map(function ($item) {
            $item->latitude = (round(100000 *  $item->latitude))/100000;
            $item->longitude = (round(100000 *  $item->longitude))/100000;
            unset($item->distance);
            return $item;
        });
    }

    public function update(Request $request){
        $updateuser = User::where('tel',$request['tel'])->first();
        $sheperds = Sheperds::all();

        $updateuser['longitude'] = $request['long'];
        $updateuser['latitude'] = $request['lat'];
        $updateuser->save();
        // select st_distance_sphere(POINT(-73.9949,40.7501), POINT( -73.9961,40.7542)) 

        $result = DB::table('sheperds')
                ->selectRaw('id,name,description,latitude,longitude, (6371 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin(radians(latitude)) ) ) AS distance', [$request['lat'], $request['long'], $request['lat']])
                ->havingRaw('distance < '.$request['rad'])
                ->get();
        $sheperds = $this->roundCoords($result);
        $result = DB::table('users')
                ->selectRaw('id,name,latitude,longitude, (6371 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin(radians(latitude)) ) ) AS distance', [$request['lat'], $request['long'], $request['lat']])
                ->where("id",'!=',$updateuser->id)
                ->havingRaw('distance < '.$request['rad'])
                ->get();
        $users = $this->roundCoords($result);       
                
        $result = DB::table('poi_locations')
                ->selectRaw('id,description,latitude,longitude, (6371 * acos( cos( radians(?) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(?) ) + sin( radians(?) ) * sin(radians(latitude)) ) ) AS distance', [$request['lat'], $request['long'], $request['lat']])
                ->havingRaw('distance < '.$request['rad'])
                ->get();
        $pois = $this->roundCoords($result);  
        $results = [
            "shepreds" => $sheperds,
            "users" => $users,
            "pois" => $pois
        ];

        return $results;
    }
}
