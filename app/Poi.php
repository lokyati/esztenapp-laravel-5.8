<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poi extends Model
{
    protected $fillable = [
        'id', 'type', 'name', 'description', 'icon', 
    ];
}
