<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoiLocation extends Model
{
    protected $fillable = [
        'id', 'pois_id', 'description', 'latitude', 'longitude', 'confirmed', 'denied'
    ];
}
