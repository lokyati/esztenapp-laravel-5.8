<!DOCTYPE html>
<html>
<head>
    <title>EsztenApp</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script src="{{ asset('js/map.js') }}" defer></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

     <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div class="menu_container" style="background-image:url('{{ asset('img/bg.svg') }}'">
        <div class="logo_container" style="background-image:url('{{ asset('img/logo.svg') }}'"></div>

        <div class="columns is-flex buttons_container">
            <div class="column is-half-mobile is-paddingless">
                <div id="terkepBtn" style="background-image:url('{{ asset('img/terkepBtn.svg') }}'"></div>
                <div id="utvonalBtn" style="background-image:url('{{ asset('img/utvonalBtn.svg') }}'"></div>
            </div>
            <div class="column is-half-mobile is-paddingless">
                <div id="infokBtn" style="background-image:url('{{ asset('img/infokBtn.svg') }}'"></div>
                <div id="profilBtn" style="background-image:url('{{ asset('img/profilBtn.svg') }}'"></div>
            </div>
        </div>
        <div id="segitsegBtn" style="background-image:url('{{ asset('img/segitsegBtn.svg') }}'"></div>
    </div>
</body>
</html>
