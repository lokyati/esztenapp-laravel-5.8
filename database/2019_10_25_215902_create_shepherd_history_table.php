<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShepherHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shepherd_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('shepherd_id');
            $table->double('latitude');
            $table->double('longitude');
            $table->timestamps();
            $table->foreign('shepherd_id')->references('id')->on('shepherd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shepherd_history');
    }
}
