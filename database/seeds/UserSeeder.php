<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("INSERT INTO `users` (`tel`, `name`, `latitude`, `longitude`) VALUES
			('+40769289047','Attila', 46.291420,25.289533),
			('+40736653550','Zolta ', 46.294406,25.290925),
			('+40744424792','Koli', 46.231420,25.269533),
			('+40748506718','Tibi ', 26.254413,25.238254)
			;");
    }
}
