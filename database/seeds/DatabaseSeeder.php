<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(POI::class);
        $this->call(SheperdSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(PoiLocationSeeder::class);
    }
}
