<?php

use Illuminate\Database\Seeder;

class PoiLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("INSERT INTO `poi_locations` (`pois_id`, `description`, `latitude`, `longitude`,`confirmed`,`denied`) VALUES
(1,'Pasztorkutyakat lattam itt', 46.289567, 25.286429, 0, 0),
(2,'Medve jar a varosban!', 46.293328, 25.287034, 0, 0),
(3,'kidolt fa', 46.294406, 25.287575, 0, 0)
;");
    }
}
