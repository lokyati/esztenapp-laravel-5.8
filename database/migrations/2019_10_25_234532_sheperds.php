<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Sheperds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sheperds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tel')->nullable;
            $table->string('name')->nullable;
            $table->longText('description')->nullable;
            $table->double('latitude')->nullable;
            $table->double('longitude')->nullable;
            $table->string('token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sheperds');
    }
}
