<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoiLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poi_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('pois_id');
            $table->longText('description')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->integer('confirmed')->nullable();
            $table->integer('denied')->nullable();
            $table->timestamps();
            $table->foreign('pois_id')->references('id')->on('pois');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poi_locations');
    }
}
