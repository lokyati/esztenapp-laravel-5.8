var map, infoWindow, myPosIcon, markersArray = [];
var refreshRate = 10;    //seconds
var oldMarkers = {};
var newMarkers = {};


var iconBase = 'images/';
var icons = {
    shepherd: {
        icon: iconBase + 'pasztorPin.png'
    },
    user: {
        icon: iconBase + 'turazoPin.png'
    },
    poi: {
        icon: iconBase + 'veszelyPin.png'
    }
};

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 17,
        streetViewControl: false,
        mapTypeId: 'satellite',
        styles: [
            {
                "featureType": "poi",
                "stylers": [
                    { "visibility": "off" }
                ]
            }
        ]
    });
    infoWindow = new google.maps.InfoWindow;

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            console.log(pos);
            map.setCenter(pos);
            var lineSymbol = {
                path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
            };
            
            // Create the polyline and add the symbol via the 'icons' property.
            myPosIcon = new google.maps.Polyline({
                path: [pos, pos],
                icons: [{
                    icon: lineSymbol,
                    offset: '100%'
                }],
                strokeColor: '#A19159',
                map: map
            });
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    navigator.geolocation.watchPosition(function(position) {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        var geolocpoint = new google.maps.LatLng(latitude, longitude);
        map.setCenter(geolocpoint);
        myPosIcon.setPath([geolocpoint, geolocpoint]);
    });

    getStuff();
    setInterval(getStuff, refreshRate * 1000);

}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                            'Error: The Geolocation service failed.' :
                            'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
    console.log("handleLocationError");
}

function addMarkers(data) {
    data.shepherds.forEach(function(obj) {
        addMarker(obj, "shepherd");
    });
    data.users.forEach(function(obj) {
        addMarker(obj, "user");
    });
    data.pois.forEach(function(obj) {
        addMarker(obj, "poi");
    });
    removeOldMarkers();
}

function addMarker(obj, type) {
    var marker;
    if(oldMarkers[type + obj.id]) {
        try {
            marker = oldMarkers[type + obj.id];
            marker.setPosition(new google.maps.LatLng(obj.latitude, obj.longitude));
            marker.description = obj.description;
            delete oldMarkers[type + obj.id];
        } catch(err) {
            console.log(err);
        }
    }
    else {
        marker = new google.maps.Marker({
            position: { 
                lat: obj.latitude,
                lng: obj.longitude
            },
            icon: icons[type].icon,
            map: map,
            description: obj.description
        });
    }
    newMarkers[type + obj.id] = marker;

    if(obj.description) {
        google.maps.event.addListener(marker, 'click', function() {
            infoWindow.setContent(marker.description);
            infoWindow.open(map, marker);
        });
    }
    markersArray.push(marker);
}

function addPOI(pointOfInterest) {

}

var routeMarkers = [];
 
 function placeMarker(location) {
     var marker = new google.maps.Marker({
         position: location, 
         map: map
     });
     return marker;
 }


function startCreatingRoute() {
    google.maps.event.addListener(map, 'click', function(event) {
        routeMarkers.push(placeMarker(event.latLng));
     }); 
    
}

function createRoute() {
    var routePlanCoordinates = [];
    routeMarkers.forEach(function(marker) {
        routePlanCoordinates.push({lat: marker.getPosition().lat(), lng: marker.getPosition().lng()});
    })
    console.log(routePlanCoordinates);
    var flightPath = new google.maps.Polyline({
        path: routePlanCoordinates,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    flightPath.setMap(map);
    routeMarkers = [];
    google.maps.event.clearListeners(map, 'click');
}

function getStuff() {
    axios.get('http://esztenapp.camelcoding.com/api/shepherds')
        .then(function (response) {
            addMarkers(response.data);
        })
        .catch(function (err) {
        })
        .finally()
}

function removeOldMarkers() {
    for (var marker in oldMarkers) {
        if (oldMarkers.hasOwnProperty(marker)) {
            oldMarkers[marker].setMap(null);
        }
    }
    oldMarkers = newMarkers;
    newMarkers = {};
}
